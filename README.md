# STAT399 - Empower Pacific
Repository for scripts and data files related to data cleaning and analysis using python 
for the capstone project for STAT399.

## Device Setup
In order to setup your device to use the files within this repository you need to install
the following onto your device:

*  [Python 3.7.4](https://www.python.org/downloads/)
*  [Git Version Control](https://git-scm.com/)
*  [PyCharm Community Edition](https://www.jetbrains.com/pycharm/download/)

Python 3.7.4 is required to run the scripts, Git version control is required to clone the 
repository to your local device and commit new files and PyCharm Community Edition 
is required for editing the scripts.

## Workspace Setup
To copy the repository to your local device:

1.  Launch PyCharm Community Edition.
2.  Select the option **"Checkout from Version Control"** and select **"Git"** from the 
drop down menu.
3.  On the bitbucket repository press the clone button located at the top right of the 
page and copy the link starting with https.
4.  In the **"Url"** field paste the link copied in step 3 and then press **"clone"**.