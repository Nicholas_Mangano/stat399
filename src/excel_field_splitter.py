import openpyxl
from openpyxl import Workbook

wb = openpyxl.load_workbook("H:/PyCharm/stat399/data/anc_pre.xlsx")
ws1 = wb.active

names = ["None", "Condoms", "Implants", "Loop/IUD", "Surgery", "Injections", "Other", "Pills", "rhythm/temperature/calender method", "Withdrawal"]
stored_values = []

print("Parsing!")
for row in range(2, ws1.max_row):
    value = (ws1.cell(row=row, column=30).value).split(', ')
    stored_values.append(value)

wb.close()

wb = Workbook()

matrix = [names]
for row in stored_values:
    temp = []
    for category in names:
        if '' not in row:
            if category in row:
                temp.append("TRUE")
            else:
                temp.append("FALSE")
        else:
            temp.append('')
    matrix.append(temp)

print("Writing!")
ws2 = wb.active
for row in range (1, len(matrix)):
    for col in range (1, len(matrix[row])):
        ws2.cell(row=row, column=col).value = matrix[row-1][col-1]

print("Saving!")
wb.save("H:/PyCharm/stat399/output/anc_pre_matrix.xlsx")
print("Done!")
