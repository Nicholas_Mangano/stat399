import openpyxl
from openpyxl import Workbook
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

wb = openpyxl.load_workbook("H:/PyCharm/stat399/data/anc_pre.xlsx")
ws1 = wb.active

stored_values = ["Jobs"]

print("Parsing Data!")
for row in range(2, ws1.max_row):
    value = (ws1.cell(row=row, column=16).value).lower().lstrip()
    if value not in stored_values:
        stored_values.append(value)

for row in range(2, ws1.max_row):
    value = (ws1.cell(row=row, column=17).value).lower().lstrip()
    if value not in stored_values:
        stored_values.append(value)


wb.close()

wb = Workbook()

print("Writing Data!")
ws2 = wb.active
for row in range (1, len(stored_values)):
    ws2.cell(row=row, column=1).value = stored_values[row-1]

print("Saving File!")
wb.save("H:/PyCharm/stat399/output/job_list_pre.xlsx")
print("Done!")
